import { Contenuto } from '../home/contenuto';


export const CONTENTS: Contenuto[] = [
    {
        id: 1,
        titolo: "PlayGround",
        sottotitolo: "Just for Fun",
        testo: "Keep in touch with our APIs. You can understand how our APIs work without any registration. Just relax and consume them.",
        avatar: "assets/rocket.png",
        immagine: "assets/city.jpg",
        bottoni: "TRY OUT!"
    },
    {
        id: 2,
        titolo: "Sandbox",
        sottotitolo: "Ready to develop",
        testo: "Keep in touch with our APIs. You can understand how our APIs work without any registration. Just relax and consume them.",
        avatar: "assets/rocket.png",
        immagine: "assets/city.jpg",
        bottoni: "CREATE ACCOUNT"
    },
    {
        id: 3,
        titolo: "Live",
        sottotitolo: "Ready to make money",
        testo: "Keep in touch with our APIs. You can understand how our APIs work without any registration. Just relax and consume them.",
        avatar: "assets/rocket.png",
        immagine: "assets/city.jpg",
        bottoni: "CREATE ACCOUNT"
    }
]