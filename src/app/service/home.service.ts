import { Injectable } from '@angular/core';
import { Contenuto } from '../home/contenuto';
import { CONTENTS } from '../mockData/mock-home';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  constructor() { }


  public getHomeContent(): Contenuto[] {
    return CONTENTS;
  }
}
