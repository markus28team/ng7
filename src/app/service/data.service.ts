import { Injectable } from '@angular/core';
import { HttpClientModule, HttpClient} from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient) { 
  }


  getUser() {
    console.log('getUser service called');
    return this.http.get("https://reqres.in/api/users");
  }

}
