import { Component, OnInit } from '@angular/core';
import { DataService } from '../service/data.service';
import { HttpClientModule  } from '@angular/common/http'

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {

  users: Object;

  constructor(private data: DataService) { }

  ngOnInit() {
    this.data.getUser().subscribe(data =>{
      this.users = data
      console.log(data)
    })
  }

}
