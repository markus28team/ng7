import { NgModule } from '@angular/core';
import {MatDividerModule, MatCardModule, MatButtonModule, MatCheckboxModule, MatToolbarModule, MatTabsModule, MatSidenavModule, MatListModule} from '@angular/material';
import {MatIconModule} from '@angular/material/icon';

@NgModule({
  imports: [MatButtonModule, MatCheckboxModule, MatToolbarModule, 
    MatIconModule, MatTabsModule, MatSidenavModule,
    MatListModule,MatCardModule, MatDividerModule],
  exports: [MatButtonModule, MatCheckboxModule,MatToolbarModule, 
    MatIconModule, MatTabsModule, MatSidenavModule,
    MatListModule, MatCardModule, MatDividerModule],
})
export class MaterialModule { }