import { Component, OnInit } from '@angular/core';
import { HomeService } from '../service/home.service';
import { Contenuto } from './contenuto';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  contenuti: Contenuto[];

  constructor(private homeService: HomeService) { }

  ngOnInit() {
    this.getContent();

  }

  private getContent() : void {
    this.contenuti = this.homeService.getHomeContent();
    console.log("recupera contenuti");
  }
}
