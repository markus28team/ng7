export class Contenuto {
    id: number;
    titolo: string;
    sottotitolo: string;
    testo: string;
    avatar: string;
    immagine: string;
    bottoni: string;
}