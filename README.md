# Ng7

## Description

Ng7 is a tutorial project with witch you can keep in touch with Angular programming.
First of all you have to download npm and then Angular CLI using the following command
`npm install -g @angular/cli`
or `npm -i -g @angular/cli`

## Create the Ng7 project

Open your favorite command prompt, in my case [Powershell](https://docs.microsoft.com/en-us/powershell/scripting/install/installing-windows-powershell?view=powershell-6), and navigate to your development folder.
After that type `ng new <projectName>`

Once angular CLI finishes all the operations, you can navigate to the project folder `cd <projectName>` and then type `code .` to open up VSCode.

## Create components

now you are ready to create your on component. you can do this using `ng generate component <componentName>`.

you will have this kind of response from Angular CLI

> $ ng generate component nav

> CREATE src/app/nav/nav.component.html (22 bytes)

> CREATE src/app/nav/nav.component.spec.ts (607 bytes)

> CREATE src/app/nav/nav.component.ts (258 bytes)

> CREATE src/app/nav/nav.component.scss (0 bytes)

> UPDATE src/app/app.module.ts (463 bytes)

...

## Development server

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.0.2.
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Technical Stuff

### Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

### Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

### Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

### Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
